# -*- coding: utf-8 -*-
import sublime_plugin
from datetime import datetime

weekday = [u"(月)", u"(火)", u"(水)", u"(木)", u"(金)", u"(土)", u"(日)"]


class InsertDateCommand(sublime_plugin.TextCommand):

    """Prints YYYY/MM/DD(A)"""
    def run(self, edit):
        self.view.insert(
            edit,
            self.view.sel()[0].begin(),
            datetime.now().strftime("%Y/%m/%d") + weekday[
                datetime.now().weekday()]
        )


class InsertTimeCommand(sublime_plugin.TextCommand):

    """Prints H:M"""
    def run(self, edit):
        self.view.insert(
            edit,
            self.view.sel()[0].begin(),
            datetime.now().strftime("%H:%M")
        )

import sublime
import webbrowser
 
class OpenUrlCommand(sublime_plugin.TextCommand):

    """Oprn URL"""
    def run(self, edit):
        s = self.view.sel()[0]
 
        # Expand selection to possible URL
        start = s.a
        end = s.b
 
        view_size = self.view.size()
        terminator = ['\t', ' ', '\"', '\'', '(', ')']
 
        while (start > 0
                and not self.view.substr(start - 1) in terminator
                and self.view.classify(start) & sublime.CLASS_LINE_START == 0):
            start -= 1
 
        while (end < view_size
                and not self.view.substr(end) in terminator
                and self.view.classify(end) & sublime.CLASS_LINE_END == 0):
            end += 1
 
        # Check if this is URL
        url = self.view.substr(sublime.Region(start, end))
        print("URL : " + url)
 
        # if url.startswith(('http://', 'https://')):
        if url.startswith(('http://', 'https://')):
            webbrowser.open_new_tab(url)
        else:
            print("not URL")


# ticketIDPrefix = 'hoge-'  # ID number format
# ticketUrlPath = 'https://com.example/'  # ticket url prefix

# class OpenTicketCommand(sublime_plugin.TextCommand):

#     """Jump to ticket tracker from ticket id"""
#     def run(self, edit):
#         s = self.view.sel()[0]
 
#         # Expand selection to possible URL
#         start = s.a
#         end = s.b
 
#         view_size = self.view.size()
#         terminator = ['\t', ' ', '\"', '\'', '(', ')']
 
#         while (start > 0
#                 and not self.view.substr(start - 1) in terminator
#                 and self.view.classify(start) & sublime.CLASS_LINE_START == 0):
#             start -= 1
 
#         while (end < view_size
#                 and not self.view.substr(end) in terminator
#                 and self.view.classify(end) & sublime.CLASS_LINE_END == 0):
#             end += 1
 
#         # Check if this is ticket id
#         url = self.view.substr(sublime.Region(start, end))
#         if url.startswith((ticketIDPrefix)):
#             webbrowser.open_new_tab(ticketUrlPath + url)
#         else:
#             print("not URL")



line = '--------------'
class InsertLineCommand(sublime_plugin.TextCommand):

    """Prints -------------- (- x 12)"""
    def run(self, edit):
        self.view.insert(
            edit,
            self.view.sel()[0].begin(),
            line
        )

